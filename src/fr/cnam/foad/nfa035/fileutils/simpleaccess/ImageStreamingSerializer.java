package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageByteArrayFrame;
/**
 * Interface ImageStreamingSerializer pour h�berger la methode serialize
 * @author herve
 * @param <T>
 *
 */
public interface ImageStreamingSerializer <S, T> {
	/**
	 * methode serialize : serialise image (type file) vers media (type ImageByteArrayFrame)
	 * @author herve
	 * @throws FileNotFoundException 
	 * @throws IOException 
	  
	 *
	 */
	void serialize(S image, T media) throws FileNotFoundException, IOException;
	// void serialize(File image, ImageByteArrayFrame media) throws FileNotFoundException, IOException;

}
