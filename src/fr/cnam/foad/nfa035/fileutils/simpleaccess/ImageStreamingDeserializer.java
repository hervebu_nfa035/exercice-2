package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageByteArrayFrame;
/**
 * 
 * @author herve
 * Streaming deserializer
 *
 */
public interface ImageStreamingDeserializer {
	
	/** 
	 * 
	 * @param media : object to deserialize
	 * @throws IOException
	 */

	void deserialize(ImageByteArrayFrame media) throws IOException;

	/**
	 *  get deserialized stream as ByteArrayOutputStream
	 * @return
	 */
	ByteArrayOutputStream getSourceOutputStream();

}
