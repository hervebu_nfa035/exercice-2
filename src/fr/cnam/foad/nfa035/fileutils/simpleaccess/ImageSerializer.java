package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;

/**
 * Interface de base pour serialiser deserialiser une image au format texte
 *
 * @author tvonstebut
 */
public interface ImageSerializer<S,M> {

    /**
     * Serialise une image en chaine de caracteres
     *
     * @param image
     * @return
     * @throws IOException
     */
    S serialize(File image) throws IOException;

    /**
     * Deserialise une chaine de caracteres en image
     *
     * @param image
     * @return
     */
    M deserialize(String image) ;

}
