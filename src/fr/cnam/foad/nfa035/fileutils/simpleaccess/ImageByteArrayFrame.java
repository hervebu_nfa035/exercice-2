package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
/**
 * 
 * @author herve
 * cette classe permet de stocker un objet de type ByteArrayOutputStream
 * et contient des methodes pour obtenir cet objet converti en OutputStream/InputStream
 */
public class ImageByteArrayFrame extends AbstractImageFrameMedia<ByteArrayOutputStream> {
/**
 * 
 * @param byteArrayOutputStream : objet contenu
 */
	public ImageByteArrayFrame(ByteArrayOutputStream byteArrayOutputStream) {
		// TODO Auto-generated constructor stub
	}
	/**
     * obtenir objet stock� dans la classe en tant que OutputStream
     *  @return un OutputStream 
     */
	@Override
	public OutputStream getEncodedImageOutput() throws IOException {
		// TODO Auto-generated method stub
		// return null;
		return (OutputStream) getChannel();
	}
	/**
     * obtenir objet stock� dans la classe en tant que InputStream
     *  @return un objet  InputStream .
     */
	@Override
	public InputStream getEncodedImageInput() throws IOException {
		// TODO Auto-generated method stub
		ByteArrayOutputStream out = getChannel();
		// ByteArrayInputStream bias = new ByteArrayInputStream(out.toByteArray());
		
		return new ByteArrayInputStream(out.toByteArray());
		// return InputStream (out.toByteArray());
		
	}
	
    
    	
    

}
