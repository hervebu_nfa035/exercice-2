package fr.cnam.foad.nfa035.fileutils.simpleaccess.test;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageDeserializerBase64StreamingImpl;
// import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
// import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageStreamingSerializer;
import org.apache.commons.codec.binary.Base64OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
// import java.io.IOException;
// import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Arrays;

// import org.apache.commons.codec.binary.Base64OutputStream;

/**
 * Classe de Test unitaire faite maison
 */
public class SimpleAccessTest {

    /**
     * Test unitaire fait maison
     *
     * @param args
     */
     @SuppressWarnings("resource")
	public static void main(String[] args) {
    	 
    	 
        try {
        	/*
            File image = new File("petite_image.png");
            ImageSerializer serializer = new ImageSerializerBase64Impl();

            // Sérialization
            String encodedImage = (String) serializer.serialize(image);
            System.out.println(splitDisplay(encodedImage,76));

            // Désérialisation
            byte[] deserializedImage = (byte[]) serializer.deserialize(encodedImage);

            // Vérifications
            //  1/ Automatique
            assert (Arrays.equals(deserializedImage, Files.readAllBytes(image.toPath())));
            System.out.println("Cette s�rialisation est bien r�versible :)");
            //  2/ Manuelle
            File extractedImage = new File("petite_image_extraite.png");
            new FileOutputStream(extractedImage).write(deserializedImage);
            System.out.println("Je peux v�rifier moi-m�me en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le r�pertoire de ce Test");
*/
    	   // File image = new File("petite_image.png");
    	   //File image = new File("superman.jpg");
    	   File image = new File("petite_image_2.png");
           ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());
                  
           // Serialisation
           ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
           serializer.serialize(image, media);

           String encodedImage = media.getEncodedImageOutput().toString();
           System.out.println("encodedImage : " + encodedImage + "\n");

           // Deserialisation
           ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
           ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);

           deserializer.deserialize(media);
           byte[] deserializedImage = ((ByteArrayOutputStream)deserializer.getSourceOutputStream()).toByteArray();
           // Verification
           // 1/ Automatique
           byte[] originImage = Files.readAllBytes(image.toPath());
           assert Arrays.equals(originImage, deserializedImage);
           System.out.println("Cette serialisation est bien reversible :)");

           //  2/ Manuelle
           File extractedImage = new File("petite_image_2_extraite.png");
           new FileOutputStream(extractedImage).write(deserializedImage);
           System.out.println("Je peux verifier moi-meme en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le r�pertoire de ce Test"); 

           // Superman declaration des variables         
           File image2 = new File("superman.jpg");
           ImageByteArrayFrame media2 = new ImageByteArrayFrame(new ByteArrayOutputStream());
           // Serialisation
           ImageStreamingSerializer serializer2 = new ImageSerializerBase64StreamingImpl();
           serializer2.serialize(image2, media2);
        // Deserialisation
           ByteArrayOutputStream deserializationOutput2 = new ByteArrayOutputStream();
           ImageStreamingDeserializer deserializer2 = new ImageDeserializerBase64StreamingImpl(deserializationOutput2);

           deserializer2.deserialize(media2);
           byte[] deserializedImage2 = ((ByteArrayOutputStream)deserializer2.getSourceOutputStream()).toByteArray();
           
           //deserializer.deserialize(media2);
           //ByteArrayOutputStream deserializationOutput2 = new ByteArrayOutputStream();
           //ImageStreamingDeserializer deserializer2 = new ImageDeserializerBase64StreamingImpl(deserializationOutput2);
           // byte[] deserializedImage2 = ((ByteArrayOutputStream)deserializer2.getSourceOutputStream()).toByteArray();
           File extractedImage2 = new File("superman_extrait2.png");
           new FileOutputStream(extractedImage2).write(deserializedImage2);
           
        } catch (Exception e) {
        // } catch (IOException e) {
            e.printStackTrace();
        } 
        // Arrays.copyOfRange(originImage,0,originImage.length-2);
}
    

    /**
     * M�thode utile pour afficher une image s�rialis�e
     *
     * @param str
     * @param chars
     * @return
     */
    private static String splitDisplay(String str, int chars){
        StringBuffer strBuf = new StringBuffer();
        int i = 0;
        strBuf.append("================== Affichage de l'image encode en Base64 ==================\n");
        for (; i+chars < str.length(); ){
            strBuf.append(str.substring(i,i+= chars));
            strBuf.append("\n");
        }
        strBuf.append(str.substring(i));
        strBuf.append("\n================================== FIN =====================================\n");

        return strBuf.toString();
    }
}
