package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
/**
 * 
 * @author herve
 * cette classe permet de stocker un objet de type T
 * et contient des methodes pour obtenir cet objet converti en OutputStream
 * 
 */
public abstract class AbstractImageFrameMedia <T> {
	    private T channel;
	    protected AbstractImageFrameMedia() {
	    }
	    /**
	     * obtenir le contenu de channel
	     *  @return  : contenu du media
	     *  
	     */
	    public T getChannel() {
	        return channel;
	    }
	    /**
	     * renseigner channel
	     *  @param channel : contenu � renseigner
	     */
	    public void setChannel(T channel) {
	        this.channel = channel;
	    }
	    /**
	     * remplir le contenu de channel
	     *  
	     * @param channel : contenu � renseigner
	     */
	    AbstractImageFrameMedia(T channel){
	        this.channel = channel;
	    }
	    /**
	     * 
	     * @return contenu sous forme de OutputStream
	     * @throws IOException : contenu du media non renseigne
	     */
	    public abstract OutputStream getEncodedImageOutput() throws IOException;
	    /**
	     * 
	     * @return contenu sous forme de InputStream
	     * @throws IOException : contenu du media non renseigne
	     */
	    public abstract InputStream getEncodedImageInput() throws IOException;
}
