package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.BufferedInputStream;
import java.lang.ClassNotFoundException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

import org.apache.commons.codec.binary.Base64OutputStream;



//public class ImageSerializerBase64StreamingImpl implements ImageStreamingSerializer {
	// AbstractStreamingImageSerializer <S,M>
public class ImageSerializerBase64StreamingImpl extends AbstractStreamingImageSerializer <File,ImageByteArrayFrame> {
	@Override
	// public void serialize(File image, ImageByteArrayFrame media) throws IOException, ClassNotFoundException {
    public void serialize(File image, ImageByteArrayFrame media) throws IOException {
		// TODO Auto-generated method stub
		// OutputStream outputStream = OutputStream(image);
		FileInputStream fis = new FileInputStream(image);
		BufferedInputStream bis = new BufferedInputStream(fis);
		byte[] fileContents = bis.readAllBytes();
		bis.close();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		
		String encodedString = Base64.getEncoder().encodeToString(fileContents);
		
		
		try {
		
		  
		  
		 
		  /* Remove following lines which provoke detection of ClassNotFoundException
		  baos.write(fileContents);
		  // OutputStream b64OutputStream = new Base64OutputStream(baos);
		  Base64OutputStream b64OutputStream = new Base64OutputStream(baos);
		  b64OutputStream.write(fileContents);
		  b64OutputStream.flush();		
		  b64OutputStream.close();
		  */
		  baos.write(encodedString.getBytes());
		  baos.flush();
		// to catch ClassNotFoundException
	     } catch (Exception e) {
        // } catch (IOException e) {
          e.printStackTrace();
          baos.write(encodedString.getBytes());
            
            }
		// {System.out.printf(e.getMessage());}
		media.setChannel(baos);
		baos.close();
		baos.flush();
		
		// image.
		// byteArrayOutputStream.
		// b64OutputStream.
	}

	@Override
	protected Object getSourceInputStream(File source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Object getSerializingStream(ImageByteArrayFrame media) {
		// TODO Auto-generated method stub
		return null;
	}

}
