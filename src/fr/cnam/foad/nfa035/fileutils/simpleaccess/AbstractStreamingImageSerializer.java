package fr.cnam.foad.nfa035.fileutils.simpleaccess;

 import java.io.IOException;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageStreamingSerializer;

public abstract class AbstractStreamingImageSerializer <S,M> implements ImageStreamingSerializer<S,M> 
{// public final void serialize(S source, M media) throws IOException ;
// { // ( getSourceInputStream(source)).transferTo(getSerializingStream(media));}

protected abstract Object getSourceInputStream(S source);

protected abstract Object getSerializingStream(M media); }


