package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageStreamingDeserializer;
/**
 * 
 * @author herve
 * Streaming deserializer implementation
 *
 */
public class ImageDeserializerBase64StreamingImpl implements ImageStreamingDeserializer {
	
	ByteArrayOutputStream baos;
	public ImageDeserializerBase64StreamingImpl(ByteArrayOutputStream deserializationOutput) {
		// TODO Auto-generated constructor stub
		baos = deserializationOutput;
	}
/**
 * deserialize a stream 
 * @param media : stream to be deserialized
 */
	@Override
	public void deserialize(ImageByteArrayFrame media) throws IOException {
		// TODO Auto-generated method stub
		String encodedImage = media.getEncodedImageOutput().toString();
		
		byte[] image = Base64.getDecoder().decode(encodedImage);
		baos.write(image);
		media.setChannel(baos);
		
	}
    /**
     *  @return deserialised stream as ByteArrayOutputStream
     */
	@Override
	public ByteArrayOutputStream getSourceOutputStream() {
		// TODO Auto-generated method stub
		return baos;
	}

}
